import React, { Component } from 'react'
import PropTypes from 'prop-types'
import store from '../../../store'
import { connect } from 'react-redux'
import * as types from '../../../action-types'
import { serverCall } from '../../../utils'

class RemoveGathering extends Component {
    constructor() {
        super()

        this._handleRemove = this._handleRemove.bind(this)
        this._removeGathering = this._removeGathering.bind(this)
        this._handleCancelation = this._handleCancelation.bind(this)
    }

    _handleRemove() {
        store.dispatch({
            type: types.POPUP_OPEN,
            variant: 'PROMPT',
            data: {
                text: 'Are you sure you want to remove ' + this.props.gathering.title + '? '
            },
            buttons: [{
                order: 1,
                caption: 'Yes',
                callback: function() {
                    this._removeGathering()
                }.bind(this)
            }, {
                order: 2,
                caption: 'Cancel',
                callback: function() {
                    store.dispatch({
                        type: types.POPUP_CLOSE
                    })
                }
            }]
        })
    }

    _removeGathering() {
        serverCall('/api/gatherings/prepare-for-removing.json', {
            gatheringId: this.props.gathering._id
        }, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.GATHERING_REMOVING_SET,
                    gatheringId: this.props.gathering._id,
                    removingDate: rsp.data.removingDate
                })

                store.dispatch({
                    type: types.POPUP_DATA_SET,
                    data: {
                        text: rsp.msg,
                        msg: ''
                    }
                })

                store.dispatch({
                    type: types.POPUP_BUTTONS_SET,
                    buttons: [{
                        order: 1,
                        caption: 'Close',
                        callback: function() {
                            store.dispatch({
                                type: types.POPUP_CLOSE
                            }) 
                        }
                    }]
                })

            } else {
                store.dispatch({
                    type: types.POPUP_DATA_SET,
                    data: {
                        text: 'Are you sure you want to remove ' + this.props.gathering.title + '?',
                        msg: rsp.msg
                    }
                })
            }
        }.bind(this))
    }

    _handleCancelation() {
        serverCall('/api/gatherings/cancel-removing.json', {
            gatheringId: this.props.gathering._id
        }, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.GATHERING_REMOVING_UNSET,
                    gatheringId: this.props.gathering._id
                })
            }
        }.bind(this))
    }

    render() {
        if (this.props.gathering.removingDate) {
            return (
                <a onClick={this._handleCancelation}>Cancel removing</a>
            )
        }

        return (
            <a onClick={this._handleRemove}>Remove it</a>
        )
    }
}

RemoveGathering.propTypes = {
    gathering: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
    return {
        gathering: store.gatheringStore.gathering
    }
}

export default connect(mapStoreToProps)(RemoveGathering)
