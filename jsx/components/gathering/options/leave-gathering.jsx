import React, { Component } from 'react'
import PropTypes from 'prop-types'
import store from '../../../store'
import { connect } from 'react-redux'
import * as types from '../../../action-types'
import { serverCall } from '../../../utils'
import { browserHistory } from 'react-router-dom'

class LeaveGathering extends Component {
    constructor() {
        super()

        this._handleLeave = this._handleLeave.bind(this)
        this._removeMeFromGathering = this._removeMeFromGathering.bind(this)
    }

    _handleLeave() {
        store.dispatch({
            type: types.POPUP_OPEN,
            variant: 'PROMPT',
            data: {
                text: 'Are you sure you want to leave ' + this.props.gathering.title + '?'
            },
            buttons: [{
                order: 1,
                caption: 'Yes',
                callback: function() {
                    this._removeMeFromGathering()
                }.bind(this)
            }, {
                order: 2,
                caption: 'Cancel',
                callback: function() {
                    store.dispatch({
                        type: types.POPUP_CLOSE
                    })
                }
            }]
        })
    }

    _removeMeFromGathering() {
        serverCall('/api/gathering-participants/remove-me.json', {
            gatheringId: this.props.gathering._id
        }, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.GATHERINGS_ONE_REMOVED,
                    gatheringId: this.props.gathering._id
                })
                store.dispatch({
                    type: types.POPUP_CLOSE
                })
                this.context.router.history.push('/app')
            } else {
                store.dispatch({
                    type: types.POPUP_DATA_SET,
                    data: {
                        text: 'Are you sure you want to leave ' + this.props.gathering.title + '?',
                        msg: rsp.msg
                    }
                })
            }
        }.bind(this))
    }

    render() {
        return (
            <a onClick={this._handleLeave}>Leave it</a>
        )
    }
}

LeaveGathering.contextTypes = {
  router: PropTypes.object.isRequired
}

LeaveGathering.propTypes = {
    gathering: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
    return {
        gathering: store.gatheringStore.gathering
    }
}

export default connect(mapStoreToProps)(LeaveGathering)
