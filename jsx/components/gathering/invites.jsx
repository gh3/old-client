import React, { Component } from 'react'
import PropTypes from 'prop-types'
import store from '../../store'
import { connect } from 'react-redux'
import { serverCall } from '../../utils'
import * as types from '../../action-types'

class Invites extends Component {
    constructor() {
        super()

        this._handleGatheringInvite = this._handleGatheringInvite.bind(this)
        this._handleRemoveInvite = this._handleRemoveInvite.bind(this)
        this._getGatheringInvites = this._getGatheringInvites.bind(this)
    }

    componentWillMount() {
        if (this.props.match.params.gatheringId) {
            this._getGatheringInvites(this.props.match.params.gatheringId)
        }
    }

    _getGatheringInvites(gatheringId) {
        serverCall('/api/gathering-invites/find-by-gathering.json', {
            gatheringId: gatheringId
        }, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.GATHERING_INVITES_RECEIVED,
                    data: rsp.data.gatheringInvites
                })
            }
        }.bind(this))
    }

    _handleGatheringInvite() {
        store.dispatch({
            type: types.POPUP_OPEN,
            variant: 'GATHERING_INVITE',
            data: {
                gatheringId: this.props.gathering._id
            }
        })
    }

    _handleRemoveInvite(gatheringInviteId) {
        serverCall('/api/gathering-invites/remove-one.json', {
            gatheringInviteId: gatheringInviteId,
            gatheringId: this.props.gathering._id
        }, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.GATHERING_INVITE_REMOVED,
                    gatheringInviteId: gatheringInviteId
                })
            }
        }.bind(this))
    }

    render() {
        return (
            <div>
                <strong>Invites:</strong>
                <div>
                    {this.props.gatheringInvites.map(function(gatheringInvite) {
                        return (
                            <div key={gatheringInvite._id}>
                                <a onClick={this._handleRemoveInvite.bind(this, gatheringInvite._id)}>X</a> <span>{gatheringInvite.phone}</span> <strong>{gatheringInvite.respond}</strong>
                            </div>
                        )
                    }.bind(this))}
                </div>
                <div>
                    <span>Want to add someone to the gathering?</span> <a onClick={this._handleGatheringInvite} style={{lineHeight: '50px'}}>Invite here!</a>
                </div>
            </div>
        )
    }
}

Invites.propTypes = {
    gathering: PropTypes.object.isRequired,
    gatheringInvites: PropTypes.array.isRequired
}

const mapStoreToProps = function(store) {
    return {
        gathering: store.gatheringStore.gathering,
        gatheringInvites: store.gatheringStore.gatheringInvites
    }
}

export default connect(mapStoreToProps)(Invites)
