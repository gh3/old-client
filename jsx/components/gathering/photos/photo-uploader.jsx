import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as types from '../../../action-types'
import { connect } from 'react-redux'
import { serverCall, generateGuid } from '../../../utils'
import store from '../../../store'

let xhrs = {}

class PhotoUploader extends Component {
    constructor() {
        super()
        this.state = {
            addedFiles: [],
            uploadProgress: {},
            newGatheringPhotos: {}
        }

        this._triggerFileInput = this._triggerFileInput.bind(this)
        this._handleFileInput = this._handleFileInput.bind(this)
        this._handleDragEnter = this._handleDragEnter.bind(this)
        this._handleDragOver = this._handleDragOver.bind(this)
        this._handleDrop = this._handleDrop.bind(this)
        this._handleFiles = this._handleFiles.bind(this)
        this._getOrientation = this._getOrientation.bind(this)
        this._presentPhoto = this._presentPhoto.bind(this)
        this._uploadPhoto = this._uploadPhoto.bind(this)
        this._updateUploadProgress = this._updateUploadProgress.bind(this)
        this._handleRemovePhoto = this._handleRemovePhoto.bind(this)
        this._handlePublish = this._handlePublish.bind(this)
        this._renderPublishButton = this._renderPublishButton.bind(this)
        this._renderPhotoPreviewThumb = this._renderPhotoPreviewThumb.bind(this)
        this._renderPhotoPreview = this._renderPhotoPreview.bind(this)
    }

    _triggerFileInput() {
        document.getElementById('file-input').click()
    }

    _handleFileInput() {
        let files = document.getElementById('file-input').files
        this._handleFiles(files)
    }

    _handleDragEnter(e) {
        e.stopPropagation()
        e.preventDefault()
    }

    _handleDragOver(e) {
        e.stopPropagation()
        e.preventDefault()
    }

    _handleDrop(e) {
        e.stopPropagation()
        e.preventDefault()
        let files = e.dataTransfer.files
        this._handleFiles(files)
    }

    _handleFiles(files) {
        let file
        for (let i = 0; i < files.length; i++) {
            file = files[i]
            file.gatheringId = this.props.gathering._id
            try {
                file.src = window.URL.createObjectURL(file)
                file.key = file.src.split('/').pop(-1)            
                this._getOrientation(file, this._presentPhoto)
            } catch (e) {
                // Browser does not support createObjectURL!
                file.key = generateGuid()
                this._presentPhoto(file)
            }
        }
    }

    _getOrientation(file, callback) {
        // http://stackoverflow.com/questions/7584794/accessing-jpeg-exif-rotation-data-in-javascript-on-the-client-side
        let reader = new FileReader()
        reader.onload = function(e) {
            let view = new DataView(e.target.result)
            if (view.getUint16(0, false) != 0xFFD8) {
                file.orientation = -2
                return callback(file)
            }
            let length = view.byteLength
            let offset = 2
            while (offset < length) {
                let marker = view.getUint16(offset, false)
                offset += 2
                if (marker == 0xFFE1) {
                    if (view.getUint32(offset += 2, false) != 0x45786966) {
                        file.orientation = -2
                        return callback(file)
                    }
                    let little = view.getUint16(offset += 6, false) == 0x4949
                    offset += view.getUint32(offset + 4, little)
                    let tags = view.getUint16(offset, little)
                    offset += 2
                    for (let i = 0; i < tags; i++) {
                        if (view.getUint16(offset + (i * 12), little) == 0x0112) {
                            file.orientation = view.getUint16(offset + (i * 12) + 8, little)
                            return callback(file)
                        }
                    }
                } else if ((marker & 0xFF00) != 0xFF00) {
                    break
                } else {
                    offset += view.getUint16(offset, false)
                }
            }
            file.orientation = -1
            return callback(file)
        }
        reader.readAsArrayBuffer(file)
    }

    _presentPhoto(file) {
        let addedFiles = this.state.addedFiles
        addedFiles.unshift(file)
        this._uploadPhoto(file)
        this.setState({
            addedFiles: addedFiles
        })
    }

    _uploadPhoto(file) {
        let form = new FormData()
        form.append('gatheringId', this.props.gathering._id)
        form.append(file.key, file)

        let xhr = new XMLHttpRequest()
        xhrs[file.key] = xhr

        xhr.open('POST', process.env.API_BASE_URL + '/api/gathering-photos/insert.json')
        xhr.setRequestHeader('Session', localStorage.getItem('session'));

        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                let newGatheringPhotos = this.state.newGatheringPhotos

                let rsp = xhr.responseText ? JSON.parse(xhr.responseText) : {'status': 'error'}

                if (rsp.status === 'ok') {
                    newGatheringPhotos[rsp.data.gatheringPhoto.fileKey] = rsp.data.gatheringPhoto
                } else {
                    newGatheringPhotos[file.key] = false    
                }

                this.setState({
                    newGatheringPhotos: newGatheringPhotos
                })
            }
        }.bind(this)

        xhr.upload.addEventListener('progress', function(e) {
            this._updateUploadProgress(e, file)
        }.bind(this))

        xhr.addEventListener('error', function(e) {
            this._updateUploadProgress(e, file)
        }.bind(this))

        xhr.send(form)
    }

    _updateUploadProgress(e, file) {
        let uploadProgress = this.state.uploadProgress
        if (e.type === 'error') {
            uploadProgress[file.key] = 'error'
        } else {
            let progressNew = e.lengthComputable ? Math.round((e.loaded / e.total) * 100) : 0
            uploadProgress[file.key] = progressNew
        }
        this.setState({
            uploadProgress: uploadProgress
        })
    }

    _handleRemovePhoto(i) {
        let addedFiles = this.state.addedFiles

        window.URL.revokeObjectURL(addedFiles[i].src)
        xhrs[addedFiles[i].key].abort()
        addedFiles.splice(i, 1)

        this.setState({
            addedFiles: addedFiles
        })
    }

    _handlePublish() {
        let addedFiles = this.state.addedFiles.reverse()
        let newGatheringPhotos = this.state.newGatheringPhotos

        let i
        let addedFile
        let gatheringPhotoId

        for (i in addedFiles) {
            addedFile = addedFiles[i]
            gatheringPhotoId = newGatheringPhotos[addedFile.key]._id

            serverCall('/api/gathering-photos/publish.json', {
                gatheringPhotoId: gatheringPhotoId
            }, function(rsp) {
                if (rsp.status === 'ok') {
                    let addedFiles = this.state.addedFiles
                    let i = addedFiles.findIndex(elm => elm.key === rsp.data.gatheringPhotoKey)

                    addedFiles.splice(i, 1)

                    this.setState({
                        addedFiles: addedFiles
                    })

                    store.dispatch({
                        type: types.GATHERING_PHOTO_INSERTED,
                        gatheringPhoto: rsp.data.gatheringPhoto
                    })
                }
            }.bind(this))
        }
    }

    _renderPublishButton() {
        let addedFiles = this.state.addedFiles

        if (!addedFiles.length) {
            return null
        }

        let uploadProgress = this.state.uploadProgress
        let newGatheringPhotos = this.state.newGatheringPhotos

        let notUploadedYetCount = 0
        let notSavedYetCount = 0

        let i
        let addedFile

        for (i in addedFiles) {
            addedFile = addedFiles[i]

            if (uploadProgress[addedFile.key] !== 100) {
                notUploadedYetCount++
            }

            if (!newGatheringPhotos[addedFile.key]) {
                notSavedYetCount++
            }
        }

        if (notUploadedYetCount) {
            return null
        }

        if (notSavedYetCount) {
            return null
        }

        return (
            <a onClick={this._handlePublish} className="pretty-btn">PUBLISH!!</a>
        )
    }

    _renderPhotoPreviewThumb(upload) {
        let className = 'photo-uploader__preview'
        if (upload.orientation && upload.orientation > 0) {
            className += ' orientation-' + upload.orientation
        }
        return (
            <div className={className} style={{backgroundImage: 'url(' + upload.src + ')'}}></div>
        )
    }

    _renderPhotoPreview(upload, i) {
        return (
            <div className="photo-uploader__added-file" key={upload.key}>
                {upload.hasOwnProperty('src') ? this._renderPhotoPreviewThumb(upload) : null}
                <div>
                    <span>{upload.name}</span>
                    <strong> {this.state.uploadProgress[upload.key] == 'error' ? 'error' : this.state.uploadProgress[upload.key]}</strong>
                    <i> {this.state.newGatheringPhotos[upload.key] ? this.state.newGatheringPhotos[upload.key]._id : null}</i>
                </div>
                <a onClick={this._handleRemovePhoto.bind(this, i)}>X</a>
            </div>
        )
    }

    render() {
        if (typeof FormData == 'undefined') {
            return (
                <div className="photo-uploader">Photo uploader is not supported on older devices. Please upgrade yourself!</div>
            )
        }

        return (
            <div className="photo-uploader">
                <input type="file" id="file-input" multiple="multiple" onChange={this._handleFileInput} style={{display: 'none'}}/>
                <div className="photo-uploader__upload-area" onClick={this._triggerFileInput} onDragEnter={this._handleDragEnter} onDragOver={this._handleDragOver} onDrop={this._handleDrop}>
                    <span>Upload Photos</span>
                </div>
                <div className="photo-uploader__added-files">
                    {this.state.addedFiles.map(this._renderPhotoPreview)}
                </div>
                {this._renderPublishButton()}
            </div>
        )
    }
}

PhotoUploader.propTypes = {
    gathering: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
    return {
        gathering: store.gatheringStore.gathering
    }
}

export default connect(mapStoreToProps)(PhotoUploader)
