import React, { Component } from 'react'
import PropTypes from 'prop-types'
import store from '../../store'
import { connect } from 'react-redux'
import { serverCall } from '../../utils'
import * as types from '../../action-types'

class Participants extends Component {
    constructor() {
        super()

        this._handleRemoveParticipant = this._handleRemoveParticipant.bind(this)
        this._getGatheringParticipants = this._getGatheringParticipants.bind(this)
    }

    componentWillMount() {
        if (this.props.match.params.gatheringId) {
            this._getGatheringParticipants(this.props.match.params.gatheringId)
        }
    }

    _getGatheringParticipants(gatheringId) {
        serverCall('/api/gathering-participants/find-by-gathering.json', {
            gatheringId: gatheringId
        }, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.GATHERING_PARTICIPANTS_RECEIVED,
                    data: rsp.data.gatheringParticipants
                })
            }
        }.bind(this))
    }

    _handleRemoveParticipant(userId) {
        serverCall('/api/gathering-participants/remove-user.json', {
            userId: userId,
            gatheringId: this.props.gathering._id
        }, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.GATHERING_PARTICIPANT_REMOVED,
                    userId: userId
                })
            }
        }.bind(this))
    }

    render() {
        return (
            <div>
                <strong>Participants:</strong>
                <div>
                    {this.props.gatheringParticipants.map(function(gatheringParticipant) {
                        if (gatheringParticipant._id === this.props.whoami._id) {
                            return (
                                <div key={gatheringParticipant._id}>
                                    <span>{gatheringParticipant.name ? gatheringParticipant.name : 'Unknown'}</span>
                                </div>
                            )
                        }

                        return (
                            <div key={gatheringParticipant._id}>
                                <a onClick={this._handleRemoveParticipant.bind(this, gatheringParticipant._id)}>X</a> <span>{gatheringParticipant.name ? gatheringParticipant.name : 'Unknown'}</span>
                            </div>
                        )
                    }.bind(this))}
                </div>
            </div>
        )
    }
}

Participants.propTypes = {
    gathering: PropTypes.object.isRequired,
    gatheringParticipants: PropTypes.array.isRequired,
    whoami: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
    return {
        gathering: store.gatheringStore.gathering,
        gatheringParticipants: store.gatheringStore.gatheringParticipants,
        whoami: store.userStore.whoami
    }
}

export default connect(mapStoreToProps)(Participants)
