import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import LeaveGathering from './options/leave-gathering'
import RemoveGathering from './options/remove-gathering'

class Options extends Component {
    _renderLeaderButtons() {
        return (
            <div>
                <div><Link to={'/gathering/' + this.props.gathering._id + '/preferences'}>Preferences</Link></div>
                <div><Link to={'/gathering/' + this.props.gathering._id + '/participants'}>Participants</Link></div>
                <div><Link to={'/gathering/' + this.props.gathering._id + '/invites'}>Invites</Link></div>
                <div><RemoveGathering /></div>
            </div>
        )
    }

    _renderParticipantButtons() {
        return (
            <div>
                <div><LeaveGathering /></div>
            </div>
        )
    }

    render() {
        let buttonsList
        if (this.props.gathering.leader) {
            buttonsList = this._renderLeaderButtons()
        } else {
            buttonsList = this._renderParticipantButtons()
        }

        return (
            <div>
                <strong>Options:</strong>
                {buttonsList}
            </div>
        )
    }
}

Options.propTypes = {
    gathering: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
    return {
        gathering: store.gatheringStore.gathering
    }
}

export default connect(mapStoreToProps)(Options)
