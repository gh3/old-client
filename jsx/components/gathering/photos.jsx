import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PhotoUploader from './photos/photo-uploader'
import * as types from '../../action-types'
import { connect } from 'react-redux'
import { serverCall } from '../../utils'
import store from '../../store'
import { findIndex } from 'lodash'

class Photos extends Component {
    constructor() {
        super()

        this.state = {
            gatheringPhotos: []
        }

        this.xhrs = {}

        this._handleEditPhotoName = this._handleEditPhotoName.bind(this)
        this._handleRemovePhotoToTrash = this._handleRemovePhotoToTrash.bind(this)
        this._removePhotoToTrash = this._removePhotoToTrash.bind(this)
        this._renderPhotoName = this._renderPhotoName.bind(this)
        this._renderUserName = this._renderUserName.bind(this)
        this._renderRemoveBtn = this._renderRemoveBtn.bind(this)
        this._getGatheringPhotos = this._getGatheringPhotos.bind(this)
        this._requestImageSource = this._requestImageSource.bind(this)
    }

    componentWillMount() {
        if (this.props.match.params.gatheringId) {
            this._getGatheringPhotos(this.props.match.params.gatheringId)
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.gatheringPhotos !== nextProps.gatheringPhotos) {

            this.setState({
                gatheringPhotos: nextProps.gatheringPhotos
            })

            nextProps.gatheringPhotos.forEach(gatheringPhoto => {
                if (!this.xhrs[gatheringPhoto._id]) {
                    this._requestImageSource(gatheringPhoto)
                }
            })
        }
    }

    _requestImageSource(gatheringPhoto) {
        // https://stackoverflow.com/questions/12710001/how-to-convert-uint8-array-to-base64-encoded-string
        let xhr = new XMLHttpRequest()
        this.xhrs[gatheringPhoto._id] = xhr

        let photoCropURL = process.env.STATIC_BASE_URL + '/photo/' + gatheringPhoto.guid + '_300x300.jpg'

        xhr.open('GET', photoCropURL, true);
        xhr.setRequestHeader('Session', localStorage.getItem('session'));
        xhr.responseType = 'arraybuffer';

        let arrayBuffer, u8, b64encoded, mimetype, gatheringPhotos, gatheringPhotoIndex

        xhr.onload = function () {
            var arrayBuffer = xhr.response
            if (arrayBuffer) {
                u8 = new Uint8Array(arrayBuffer)
                b64encoded = btoa(String.fromCharCode.apply(null, u8))
                mimetype = 'image/png'
                gatheringPhoto.src = 'data:' + mimetype + ';base64,' + b64encoded

                gatheringPhotos = this.state.gatheringPhotos
                gatheringPhotoIndex = findIndex(gatheringPhotos, { _id: gatheringPhoto._id })
                gatheringPhotos[gatheringPhotoIndex] = gatheringPhoto

                this.setState({
                    gatheringPhotos: gatheringPhotos
                })
            }
        }.bind(this)

        xhr.send(null);
    }

    _getGatheringPhotos(gatheringId) {
        serverCall('/api/gathering-photos/find-by-gathering.json', {
            gatheringId: gatheringId
        }, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.GATHERING_PHOTOS_RECEIVED,
                    data: rsp.data.gatheringPhotos
                })
            }
        }.bind(this))

        serverCall('/api/gathering-participants/find-by-gathering.json', {
            gatheringId: gatheringId
        }, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.GATHERING_PARTICIPANTS_RECEIVED,
                    data: rsp.data.gatheringParticipants
                })
            }
        }.bind(this))
    }

    _handleEditPhotoName(gatheringPhotoId, photoName) {
        store.dispatch({
            type: types.POPUP_OPEN,
            variant: 'EDIT_PHOTO_NAME',
            data: {
                gatheringPhotoId: gatheringPhotoId,
                photoName: photoName
            }
        })
    }

    _handleRemovePhotoToTrash(gatheringPhoto) {
        store.dispatch({
            type: types.POPUP_OPEN,
            variant: 'PROMPT',
            data: {
                text: 'Are you sure you want to remove this photo?',
                gatheringPhoto: gatheringPhoto
            },
            buttons: [{
                order: 1,
                caption: 'Yes',
                callback: function() {
                    this._removePhotoToTrash()
                }.bind(this)
            }, {
                order: 2,
                caption: 'Cancel',
                callback: function() {
                    store.dispatch({
                        type: types.POPUP_CLOSE
                    })
                }
            }]
        })
    }

    _removePhotoToTrash() {
        let isGatheringLeader = this.props.gathering.userId === this.props.whoami._id ? true : false
        let isPhotoOwner = this.props.popupData.gatheringPhoto.userId === this.props.whoami._id ? true : false

        let url
        let data

        if (isPhotoOwner) {
            url = '/api/gathering-photos/remove-to-trash.json'
            data = {
                gatheringPhotoId: this.props.popupData.gatheringPhoto._id
            }
        } else if (isGatheringLeader) {
            url = '/api/gathering-photos/remove-to-trash-as-leader.json'
            data = {
                gatheringPhotoId: this.props.popupData.gatheringPhoto._id,
                gatheringId: this.props.gathering._id
            }
        } else {
            // Not photo owner or gathering leader, no fun!
        }

        serverCall(url, data, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.GATHERING_PHOTO_REMOVED,
                    gatheringPhotoId: rsp.data.gatheringPhotoId
                })
                store.dispatch({
                    type: types.POPUP_CLOSE
                })
            }
        }.bind(this))
    }

    _renderPhotoName(gatheringPhoto) {
        let photoName = gatheringPhoto.customName ? gatheringPhoto.customName : gatheringPhoto.name
        let isPhotoOwner = gatheringPhoto.userId === this.props.whoami._id ? true : false

        if (isPhotoOwner) {
            return (
                <a className="gathering-photo-name" onClick={this._handleEditPhotoName.bind(this, gatheringPhoto._id, photoName)}>
                    <strong>{photoName}</strong>
                </a>
            )
        } else {
            return (
                <strong className="gathering-photo-name">{photoName}</strong>
            )
        }
    }

    _renderUserName(gatheringPhoto) {
        if (!this.props.gatheringParticipants.length) {
            return null
        }

        let i = this.props.gatheringParticipants.findIndex(elm => elm._id === gatheringPhoto.userId)
        let userName = i > -1 && this.props.gatheringParticipants[i].name ? this.props.gatheringParticipants[i].name : 'Unknown'

        return (
            <span>{userName}: </span>
        )
    }

    _renderRemoveBtn(gatheringPhoto) {
        let isGatheringLeader = this.props.gathering.userId === this.props.whoami._id ? true : false
        let isPhotoOwner = gatheringPhoto.userId === this.props.whoami._id ? true : false

        if (isGatheringLeader || isPhotoOwner) {
            return (
                <a onClick={this._handleRemovePhotoToTrash.bind(this, gatheringPhoto)}>X</a>
            )
        }

        return null
    }

    render() {
        return (
            <div>
                <PhotoUploader />
                <div>
                    {this.state.gatheringPhotos.map(function(gatheringPhoto) {
                        return (
                            <div key={gatheringPhoto._id}>
                                <img src={gatheringPhoto.src} className="gathering-photo" />
                                {this._renderUserName(gatheringPhoto)} {this._renderPhotoName(gatheringPhoto)} {this._renderRemoveBtn(gatheringPhoto)}
                            </div>
                        )
                    }.bind(this))}
                </div>
            </div>
        )
    }
}

Photos.propTypes = {
    gathering: PropTypes.object.isRequired,
    gatheringPhotos: PropTypes.array.isRequired,
    gatheringParticipants: PropTypes.array.isRequired,
    whoami: PropTypes.object.isRequired,
    popupData: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
    return {
        gathering: store.gatheringStore.gathering,
        gatheringPhotos: store.gatheringStore.gatheringPhotos,
        gatheringParticipants: store.gatheringStore.gatheringParticipants,
        whoami: store.userStore.whoami,
        popupData: store.popupStore.data
    }
}

export default connect(mapStoreToProps)(Photos)
