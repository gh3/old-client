import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { serverCall } from '../../utils'
import { Link } from 'react-router-dom'
import store from '../../store'
import * as types from '../../action-types'
import { connect } from 'react-redux'

class Password extends Component {
    constructor() {
        super()
        this.state = {
            msg: ''
        }

        this._handleRecover = this._handleRecover.bind(this)
        this._handleAuthData = this._handleAuthData.bind(this)
    }

    componentDidMount() {
        if (this.props.auth.areacode) {
            this.refs.areacode.value = this.props.auth.areacode
        }
        if (this.props.auth.phone) {
            this.refs.phone.value = this.props.auth.phone
        }
    }

    _handleRecover(e) {
        e.preventDefault
        if (e.keyCode && e.keyCode !== 13) {
            return
        }

        this.setState({
            msg: ''
        })

        serverCall('/api/auth/password-recovery.json', {
            areacode: this.refs.areacode.value,
            phone: this.refs.phone.value
        }, function(rsp) {
            this.setState({
                msg: rsp.msg
            })

            if (rsp.status === 'ok') {
                this.refs.phone.value = ''
            }
        }.bind(this))
    }

    _handleAuthData() {
        store.dispatch({
            type: types.USER_AUTH_STORE,
            data: {
                areacode: this.refs.areacode.value,
                phone: this.refs.phone.value
            }
        })
    }

    _renderAreacodes() {
        let areacodesOptions = this.props.areacodes.map((areacode) => {
            return (
                <option key={areacode._id} value={areacode.areacode}>{areacode.title}</option>
            )
        })

        return (
            <select className="form-block" ref="areacode" onChange={this._handleAuthData}>{areacodesOptions}</select>
        )
    }

    render() {
        return (
            <div>
                <div>
                    <label>
                        <span>Areacode</span>
                        {this._renderAreacodes()} 
                    </label>
                </div>
                <div>
                    <label>
                        <span>Phone</span>
                        <input className="form-block" type="tel" placeholder="Phone" ref="phone" onKeyDown={this._handleRecover} onBlur={this._handleAuthData} />
                    </label>
                </div>
                <div>
                    <a className="pretty-btn" onClick={this._handleRecover}>Recover password</a>
                </div>
                <div>{this.state.msg}</div>
                <div style={{paddingTop: '10px'}}>
                    <small>
                        <Link to="/login">Back to Login</Link>
                    </small>
                </div>
            </div>
        )
    }
}

Password.propTypes = {
    auth: PropTypes.object.isRequired,
    areacodes: PropTypes.array.isRequired
}

const mapStoreToProps = function(store) {
    return {
        auth: store.userStore.auth,
        areacodes: store.areacodesStore
    }
}

export default connect(mapStoreToProps)(Password)
