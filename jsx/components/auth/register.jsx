import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { serverCall } from '../../utils'
import { browserHistory } from 'react-router-dom'
import store from '../../store'
import * as types from '../../action-types'
import { connect } from 'react-redux'

class Register extends Component {
    constructor() {
        super()
        this.state = {
            msg: ''
        }

        this._postRegister = this._postRegister.bind(this)
        this._handleRegister = this._handleRegister.bind(this)
        this._openConfirmation = this._openConfirmation.bind(this)
        this._handleAuthData = this._handleAuthData.bind(this)
    }

    componentDidMount() {
        if (this.props.auth.areacode) {
            this.refs.areacode.value = this.props.auth.areacode
        }
        if (this.props.auth.phone) {
            this.refs.phone.value = this.props.auth.phone
        }
        if (this.props.auth.password) {
            this.refs.password.value = this.props.auth.password
        }
    }

    _postRegister(fromPopup) {
        let data = {
            areacode: this.refs.areacode.value,
            phone: this.refs.phone.value,
            password: this.refs.password.value
        }

        if (fromPopup) {
            data.doublecheck = true
        }

        serverCall('/api/auth/register.json', data, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.USER_SESSION_SET,
                    session: rsp.data.session
                })
                this.props.history.push('/app')

                if (fromPopup) {
                    store.dispatch({
                        type: types.POPUP_CLOSE
                    })
                }

            } else if (rsp.status === 'error' && rsp.error === 'doublecheck') {
                this._openConfirmation(rsp.data.phoneSMS)

            } else {
                if (fromPopup) {
                    store.dispatch({
                        type: types.POPUP_CLOSE
                    })
                }

                this.setState({
                    msg: rsp.msg
                })
            }
        }.bind(this))
    }

    _handleRegister(e) {
        e.preventDefault
        if (e.keyCode && e.keyCode !== 13) {
            return
        }

        this.setState({
            msg: ''
        })

        this._postRegister(false)
    }

    _openConfirmation(phoneSMS) {
        store.dispatch({
            type: types.POPUP_OPEN,
            variant: 'REGISTRATION_DOUBLECHECK',
            data: {
                phoneSMS: phoneSMS
            },
            buttons: [{
                order: 1,
                caption: 'OK',
                callback: function() {
                    this._postRegister(true)
                }.bind(this)
            }, {
                order: 2,
                caption: 'Edit',
                callback() {
                    store.dispatch({
                        type: types.POPUP_CLOSE
                    })
                }
            }]
        })
    }

    _handleAuthData() {
        store.dispatch({
            type: types.USER_AUTH_STORE,
            data: {
                areacode: this.refs.areacode.value,
                phone: this.refs.phone.value,
                password: this.refs.password.value
            }
        })
    }

    _renderAreacodes() { 
        let areacodesOptions = this.props.areacodes.map((areacode) => { 
            return ( 
                <option key={areacode._id} value={areacode.areacode}>{areacode.title}</option> 
            ) 
        }) 
 
        return ( 
            <select className="form-block" ref="areacode" onChange={this._handleAuthData}>{areacodesOptions}</select> 
        ) 
    }

    render() {
        return (
            <div>
                <div>
                    <label>
                        <span>Areacode</span>
                        {this._renderAreacodes()} 
                    </label>
                </div>
                <div>
                    <label>
                        <span>Phone</span>
                        <input className="form-block" type="tel" placeholder="Phone" ref="phone" onKeyDown={this._handleRegister} onBlur={this._handleAuthData} />
                    </label>
                </div>
                <div>
                    <label>
                        <span>Password</span>
                        <input className="form-block" type="password" placeholder="Password" ref="password" onKeyDown={this._handleRegister} onBlur={this._handleAuthData} />
                    </label>
                </div>
                <div>

                    <a className="pretty-btn" onClick={this._handleRegister}>Register</a>
                </div>
                <div>{this.state.msg}</div>
            </div>
        )
    }
}

Register.propTypes = {
    auth: PropTypes.object.isRequired,
    areacodes: PropTypes.array.isRequired
}

const mapStoreToProps = function(store) {
    return {
        auth: store.userStore.auth,
        areacodes: store.areacodesStore
    }
}

export default connect(mapStoreToProps)(Register)
    