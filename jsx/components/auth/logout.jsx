import React, { Component } from 'react'
import store from '../../store'
import * as types from '../../action-types'
import { browserHistory } from 'react-router-dom'

class Logout extends Component {
    componentWillMount() {
        store.dispatch({
            type: types.USER_SESSION_UNSET
        })
        this.props.history.push('/login')
    }

    render() {
        return (
            <div>Logout in process...</div>
        )
    }
}

export default Logout
