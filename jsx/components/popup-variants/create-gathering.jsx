import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as types from '../../action-types'
import store from '../../store'
import PopupButtons from '../popup/popup-buttons'
import PopupClose from '../popup/popup-close'
import { browserHistory } from 'react-router-dom'
import { serverCall } from '../../utils'

class CreateGathering extends Component {
    constructor() {
        super()
        this.state = {
            msg: ''
        }

        this._insertGathering = this._insertGathering.bind(this)
    }

    componentWillMount() {
        store.dispatch({
            type: types.POPUP_BUTTONS_SET,
            buttons: [{
                order: 1,
                caption: 'OK',
                callback: this._insertGathering
            }, {
                order: 2,
                caption: 'Cancel',
                callback: function() {
                    store.dispatch({
                        type: types.POPUP_CLOSE
                    })
                }
            }]
        })
    }

    _insertGathering() {
        if (!this.refs.title.value) {
            this.setState({
                msg: 'Please enter title!'
            })
            return
        }

        this.setState({
            msg: ''
        })

        serverCall('/api/gatherings/insert.json', {
            title: this.refs.title.value
        }, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.POPUP_CLOSE
                })
                this.context.router.history.push('/gathering/' + rsp.data.gathering._id)

            } else if (rsp.status === 'error') {
                this.setState({
                    msg: rsp.msg
                })
            }
        }.bind(this))
    }

    render() {
        return (
            <div className="popup--create-gathering">
                <PopupClose />
                <div className="popup__content">
                    <div>Enter gathering title:</div>
                    <input className="form-block" type="text" ref="title" placeholder="Gathering title " />
                    <div>Hit OK when done, or click Cancel if you changed your mind.</div>
                    <div>{this.state.msg}</div>
                </div>
                <PopupButtons />
            </div>
        )
    }
}

CreateGathering.contextTypes = {
  router: PropTypes.object.isRequired
}

export default CreateGathering
