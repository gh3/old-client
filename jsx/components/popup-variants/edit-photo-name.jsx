import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as types from '../../action-types'
import store from '../../store'
import { connect } from 'react-redux'
import PopupButtons from '../popup/popup-buttons'
import PopupClose from '../popup/popup-close'
import { serverCall } from '../../utils'

class EditPhotoName extends Component {
    constructor() {
        super()
        this.state = {
            msg: ''
        }

        this._handleCancel = this._handleCancel.bind(this)
        this._updatePhotoName = this._updatePhotoName.bind(this)
    }

    componentWillMount() {
        store.dispatch({
            type: types.POPUP_BUTTONS_SET,
            buttons: [{
                order: 1,
                caption: 'Save',
                callback: this._updatePhotoName
            }, {
                order: 2,
                caption: 'Cancel',
                callback: this._handleCancel
            }]
        })
    }

    componentDidMount() {
        this.refs.name.select()
    }

    _handleCancel() {
        store.dispatch({
            type: types.POPUP_CLOSE
        })
    }

    _updatePhotoName() {
        if (!this.refs.name.value) {
            this.setState({
                msg: 'Please enter photo name!'
            })
            return
        }

        if (this.refs.name.value === this.props.data.photoName) {
            this.setState({
                msg: 'New name is the same as old one!'
            })
            return
        }

        this.setState({
            msg: ''
        })

        serverCall('/api/gathering-photos/update-custom-name.json', {
            customName: this.refs.name.value,
            gatheringPhotoId: this.props.data.gatheringPhotoId
        }, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.GATHERING_PHOTO_NAME_UPDATED,
                    data: {
                        customName: this.refs.name.value,
                        gatheringPhotoId: this.props.data.gatheringPhotoId
                    }
                })

                store.dispatch({
                    type: types.POPUP_CLOSE
                })
            } else if (rsp.status === 'error') {
                this.setState({
                    msg: rsp.msg
                })
            }
        }.bind(this))
    }

    render() {
        return (
            <div className="popup--edit-photo-name">
                <PopupClose />
                <div className="popup__content">
                    <div>Custom photo name:</div>
                    <input className="form-block" type="text" ref="name" defaultValue={this.props.data.photoName} />
                    <div>{this.state.msg}</div>
                </div>
                <PopupButtons />
            </div>
        )
    }
}

EditPhotoName.propTypes = {
    data: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
    return {
        data: store.popupStore.data
    }
}

export default connect(mapStoreToProps)(EditPhotoName)
