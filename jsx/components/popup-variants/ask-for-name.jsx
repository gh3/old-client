import React, { Component } from 'react'
import * as types from '../../action-types'
import store from '../../store'
import PopupButtons from '../popup/popup-buttons'
import PopupClose from '../popup/popup-close'
import { serverCall } from '../../utils'

class AskForName extends Component {
    constructor() {
        super()
        this.state = {
            mode: 'STEP_1',
            msg: ''
        }

        this._handleStep2 = this._handleStep2.bind(this)
        this._handleStep3 = this._handleStep3.bind(this)
        this._handleNextName = this._handleNextName.bind(this)
        this._randomNinjaName = this._randomNinjaName.bind(this)
        this._randomIntFromInterval = this._randomIntFromInterval.bind(this)
        this._capitalizeFirstLetter = this._capitalizeFirstLetter.bind(this)
        this._updateName = this._updateName.bind(this)
    }

    componentWillMount() {
        store.dispatch({
            type: types.POPUP_BUTTONS_SET,
            buttons: [{
                order: 1,
                caption: 'OK',
                callback: this._updateName
            }, {
                order: 2,
                caption: 'I hate names',
                callback: this._handleStep2
            }]
        })
    }

    _handleStep2() {
        store.dispatch({
            type: types.POPUP_BUTTONS_SET,
            buttons: [{
                order: 1,
                caption: 'OK',
                callback: this._updateName
            }, {
                order: 2,
                caption: 'No',
                callback: this._handleStep3
            }]
        })

        this.setState({
            mode: 'STEP_2'
        })
    }

    _handleStep3() {
        store.dispatch({
            type: types.POPUP_BUTTONS_SET,
            buttons: [{
                order: 1,
                caption: 'OK',
                callback: this._updateName
            }, {
                order: 2,
                caption: 'Next',
                callback: this._handleNextName
            }]
        })

        this.setState({
            mode: 'STEP_3'
        })

        this._handleNextName()
    }

    _handleNextName() {
        this.refs.name.value = this._randomNinjaName()
    }

    _randomNinjaName() {
        let syllables = ['ka', 'zu', 'mi', 'te', 'ku', 'lu', 'ji', 'ri', 'ki', 'zu', 'me', 'ta', 'rin', 'to', 'mo', 'no', 'ke', 'shi', 'ari', 'chi', 'do', 'ru', 'mei', 'na', 'fu', 'zu']
        let numberOfSyllables = this._randomIntFromInterval(2,4)
        let ninjaName = ''
        for (let i = 0; i < numberOfSyllables; i++) {
            ninjaName = ninjaName + syllables[Math.floor(Math.random() * syllables.length)]
        }
        ninjaName = this._capitalizeFirstLetter(ninjaName)
        return ninjaName
    }

    _randomIntFromInterval(min, max) {
        return Math.floor(Math.random()*(max-min+1)+min)
    }

    _capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1)
    }

    _updateName() {
        if (!this.refs.name.value) {
            this.setState({
                msg: 'Please enter name!'
            })
            return
        }

        this.setState({
            msg: ''
        })

        serverCall('/api/user/update-name.json', {
            name: this.refs.name.value
        }, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.USER_NAME_UPDATED,
                    name: this.refs.name.value
                })

                store.dispatch({
                    type: types.POPUP_CLOSE
                })
            } else if (rsp.status === 'error') {
                this.setState({
                    msg: rsp.msg
                })
            }
        }.bind(this))
    }

    render() {
        let asking
        if (this.state.mode === 'STEP_1') {
            asking = 'No one knows who you are yet! Please tell us your name:'
        } else if (this.state.mode === 'STEP_2') {
            asking = 'Do you have a nickname. Your friends will recognize you like this:'
        } else if (this.state.mode === 'STEP_3') {
            asking = 'I have an idea. You can have a Ninja name! This one suits you:'
        }

        return (
            <div className="popup--ask-for-name">
                <PopupClose />
                <div className="popup__content">
                    <div className="active">Hey Snowflake!!</div>
                    <div>{asking}</div>
                    <span>New name:</span>
                    <input placeholder="New name" className="form-block" type="text" ref="name" />
                    <div>Hit OK when done.</div>
                    <div>{this.state.msg}</div>
                </div>
                <PopupButtons />
            </div>
        )
    }   
}

export default AskForName
