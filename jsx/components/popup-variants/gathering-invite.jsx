import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as types from '../../action-types'
import store from '../../store'
import PopupButtons from '../popup/popup-buttons'
import PopupClose from '../popup/popup-close'
import { serverCall } from '../../utils'
import { connect } from 'react-redux'

class GatheringInvite extends Component {
    constructor() {
        super()
        this.state = {
            msg: ''
        }

        this._handleInvite = this._handleInvite.bind(this)
        this._renderAreacodes = this._renderAreacodes.bind(this)
    }

    componentWillMount() {
        store.dispatch({
            type: types.POPUP_BUTTONS_SET,
            buttons: [{
                order: 1,
                caption: 'Invite',
                callback: this._handleInvite
            }, {
                order: 2,
                caption: 'Cancel',
                callback: function() {
                    store.dispatch({
                        type: types.POPUP_CLOSE
                    })
                }
            }]
        })
    }

    _handleInvite(e) {
        e.preventDefault
        if (e.keyCode && e.keyCode !== 13) {
            return
        }

        if (!this.refs.areacode.value) {
            this.setState({
                msg: 'Please enter areacode!'
            })
            return
        }

        if (!this.refs.phone.value) {
            this.setState({
                msg: 'Please enter phone!'
            })
            return
        }

        this.setState({
            msg: ''
        })

        serverCall('/api/gathering-invites/send-via-sms.json', {
            areacode: this.refs.areacode.value,
            phone: this.refs.phone.value,
            gatheringId: this.props.data.gatheringId
        }, function(rsp) {
            if (rsp.status === 'ok') {
                this.setState({
                    msg: rsp.msg
                })

                this.refs.phone.value = ''

                store.dispatch({
                    type: types.GATHERING_INVITE_INSERTED,
                    gatheringInvite: rsp.data.gatheringInvite
                })

                store.dispatch({
                    type: types.POPUP_BUTTONS_SET,
                    buttons: [{
                        order: 1,
                        caption: 'Invite another',
                        callback: this._handleInvite
                    }, {
                        order: 2,
                        caption: 'Close',
                        callback: function() {
                            store.dispatch({
                                type: types.POPUP_CLOSE
                            })
                        }
                    }]
                })

            } else if (rsp.status === 'error') {
                this.setState({
                    msg: rsp.msg
                })
            }
        }.bind(this))
    }

    _renderAreacodes() {
        let areacodesOptions = this.props.areacodes.map((areacode) => {
            return (
                <option key={areacode._id} value={areacode.areacode}>{areacode.title}</option>
            )
        })

        return (
            <select className="form-block" ref="areacode">{areacodesOptions}</select>
        )
    }

    render() {
        return (
            <div className="popup--gathering-invite">
                <PopupClose />
                <div className="popup__content">
                    <div>
                        <div>
                            <label>
                                <span>Areacode</span>
                                {this._renderAreacodes()}
                            </label>
                        </div>
                        <div>
                            <label>
                                <span>Phone</span>
                                <input type="tel" ref="phone" onKeyDown={this._handleInvite} className="form-block" placeholder="Phone" />
                            </label>
                        </div>
                        <div>{this.state.msg}</div>
                    </div>
                </div>
                <PopupButtons />
            </div>
        )
    }
}

GatheringInvite.propTypes = {
    data: PropTypes.object.isRequired,
    areacodes: PropTypes.array.isRequired
}

const mapStoreToProps = function(store) {
    return {
        data: store.popupStore.data,
        areacodes: store.areacodesStore
    }
}

export default connect(mapStoreToProps)(GatheringInvite)
