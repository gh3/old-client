import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as types from '../../action-types'
import store from '../../store'
import Prompt from '../popup-variants/prompt'
import CreateGathering from '../popup-variants/create-gathering'
import RegisterDoublecheck from '../popup-variants/register-doublecheck'
import GatheringInvite from '../popup-variants/gathering-invite'
import AskForName from '../popup-variants/ask-for-name'
import EditPhotoName from '../popup-variants/edit-photo-name'

class Popup extends Component {
    componentDidMount() {
        window.onpopstate = this._handleWindowOnPopState
    }

    _handleWindowOnPopState() {
        if (this.props && this.props.variant) {
            store.dispatch({
                type: types.POPUP_CLOSE
            })
        }
    }

    render() {
        if (!this.props.visible) {
            return null
        }

        let popupVariant = ''

        switch(this.props.variant) {
            case 'PROMPT':
                popupVariant = <Prompt />
                break
            case 'REGISTRATION_DOUBLECHECK':
                popupVariant = <RegisterDoublecheck />
                break
            case 'CREATE_GATHERING':
                popupVariant = <CreateGathering />
                break
            case 'GATHERING_INVITE':
                popupVariant = <GatheringInvite />
                break
            case 'ASK_FOR_NAME':
                popupVariant = <AskForName />
                break
            case 'EDIT_PHOTO_NAME':
                popupVariant = <EditPhotoName />
                break
            default:
                // Popup variant not defined!
        }

        return (
            <div className="popup">
                {popupVariant}
            </div>
        )
    }
}

Popup.propTypes = {
    visible: PropTypes.bool.isRequired,
    variant: PropTypes.string.isRequired
}

const mapStoreToProps = function(store) {
    return {
        visible: store.popupStore.visible,
        variant: store.popupStore.variant
    }
}

export default connect(mapStoreToProps)(Popup)
