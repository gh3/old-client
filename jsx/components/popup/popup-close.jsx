import React, { Component } from 'react'
import store from '../../store'
import * as types from '../../action-types'

class PopupClose extends Component {
    _handleClose() {
        store.dispatch({
            type: types.POPUP_CLOSE
        })
    }

    render() {
        return (
            <a className="popup__close" onClick={this._handleClose}>X</a>
        )
    }
}

export default PopupClose
