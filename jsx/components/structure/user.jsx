import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { serverCall } from '../../utils'
import store from '../../store'
import * as types from '../../action-types'
import { connect } from 'react-redux'

class User extends Component {
    componentWillMount() {
        if (this.props.session) {
            this._requestWhoamiUserData()
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.session && nextProps.session !== this.props.session) {
            this._requestWhoamiUserData()
        }
    }

    _requestWhoamiUserData() {
        serverCall('/api/auth/whoami.json', {}, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.USER_WHOAMI_RECEIVED,
                    whoami: rsp.data.whoami
                })
            }
        }.bind(this))
    }

    _handleNameAsking() {
        store.dispatch({
            type: types.POPUP_OPEN,
            variant: 'ASK_FOR_NAME'
        })
    }

    render() {
        if (!this.props.whoami._id) {
            return null
        }

        let user
        if (this.props.whoami.name) {
            user = this.props.whoami.name
        } else {
            user = <a onClick={this._handleNameAsking}>Snowflake</a>
        }

        return (
            <span style={{ float: 'right' }}>{ user }</span>
        )
    }
}

User.propTypes = {
    session: PropTypes.string.isRequired,
    whoami: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
    return {
        session: store.userStore.session,
        whoami: store.userStore.whoami
    }
}

export default connect(mapStoreToProps)(User)
