import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Popup from '../popup/popup'
import { serverCall } from '../../utils'
import store from '../../store'
import * as types from '../../action-types'
import Menu from './menu'
import Header from './header'
import { connect } from 'react-redux'
import { Switch, Route } from 'react-router-dom'
import routes from '../../routes'
import Logout from '../auth/logout'

class MainLayout extends Component {
    componentDidMount() {
        serverCall('/api/areacodes/find.json', {}, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.AREACODES_DATA_RECEIVED,
                    areacodes: rsp.data.areacodes
                })
            }
        }.bind(this))
    }

    componentWillMount() {
        if (this.props.location.pathname === '/') {
            if (this.props.session) {
                this.props.history.push('/app')
            } else {
                this.props.history.push('/login')
            }
        }
    }

    renderRoute(route) {
        if (route.type === 'SESSION' && !this.props.session) {
            return (
                <Route key={route.id} path={route.path} component={Logout} exact={!!route.exact} />
            )
        }

        return (
            <Route key={route.id} path={route.path} component={route.component} exact={!!route.exact} />
        )
    }

    render() {
        return (
            <div className="main-layout">
                <Header />
                <Menu />
                <Switch>
                    {routes.map(route => this.renderRoute(route))}
                </Switch>
                <Popup />
            </div>
        )
    }
}

MainLayout.propTypes = {
    session: PropTypes.string.isRequired
}

const mapStoreToProps = function(store) {
    return {
        session: store.userStore.session
    }
}

export default connect(mapStoreToProps)(MainLayout)
