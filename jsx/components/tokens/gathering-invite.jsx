import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { serverCall } from '../../utils'
import * as types from '../../action-types'
import store from '../../store'
import { browserHistory } from 'react-router-dom'
import { connect } from 'react-redux'
import { isEmpty } from 'lodash'

class GatheringInvite extends Component {
    constructor() {
        super()
        this.state = {
            mode: 'INITIALIZATION',
            msg: 'Processing token in progress...',
            gatheringInvite: false,
            gathering: false,
            respond: ''
        }

        this._registerWithToken = this._registerWithToken.bind(this)
        this._loginWithToken = this._loginWithToken.bind(this)
        this._respondOnToken = this._respondOnToken.bind(this)
        this._handleContinueToApp = this._handleContinueToApp.bind(this)
    }

    componentWillMount() {
        store.dispatch({
            type: types.STORE_RESET
        })
    }

    componentWillReceiveProps(nextProps) {
        if (isEmpty(this.props.areacodes) && !isEmpty(nextProps.areacodes)) {
            serverCall('/api/gathering-invites/check-belonging.json', {
                token: this.props.match.params.token
            }, function(rsp) {
                this.setState({
                    mode: 'CHECK_BELONGING_RSP'
                })
                if (rsp.status === 'ok') {
                    this.setState({
                        gatheringInvite: rsp.data.gatheringInvite,
                        gathering: rsp.data.gathering
                    })
                    if (rsp.data.user) {
                        this._loginWithToken(rsp.data.gatheringInvite)
                    } else {
                        this._registerWithToken(rsp.data.gatheringInvite)
                    }
                } else {
                    this.setState({
                        msg: rsp.msg
                    })
                }
            }.bind(this))
        }
    }

    _registerWithToken(gatheringInvite) {
        let i = this.props.areacodes.findIndex(elm => elm._id === gatheringInvite.areacodeId)

        serverCall('/api/gathering-invites/register.json', {
            areacode: this.props.areacodes[i].areacode,
            phone: gatheringInvite.phone,
            token: this.props.match.params.token,
            doublecheck: 1
        }, function(rsp) {
            this.setState({
                mode: 'REGISTER_RSP'
            })
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.USER_SESSION_SET,
                    session: rsp.data.session
                })

                this._respondOnToken('accepted')
            } else {
                this.setState({
                    msg: rsp.msg
                })
            }
        }.bind(this))
    }

    _loginWithToken(gatheringInvite) {
        let i = this.props.areacodes.findIndex(elm => elm._id === gatheringInvite.areacodeId)

        serverCall('/api/gathering-invites/login.json', {
            areacode: this.props.areacodes[i].areacode,
            phone: gatheringInvite.phone,
            token: this.props.match.params.token
        }, function(rsp) {
            this.setState({
                mode: 'LOGIN_RSP'
            })
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.USER_SESSION_SET,
                    session: rsp.data.session
                })

                if (!gatheringInvite.respond) {
                    this.setState({
                        mode: 'ASK_FOR_RESPOND',
                        msg: ''
                    })
                } else if (gatheringInvite.respond === 'rejected') {
                    this.setState({
                        msg: 'Gathering invite was already rejected!'
                    })
                } else if (gatheringInvite.respond === 'accepted') {
                    this.context.router.history.push('/gathering/' + this.state.gatheringInvite.gatheringId)
                }

            } else {
                this.setState({
                    mode: 'LOGIN_RSP',
                    msg: rsp.msg
                })
            }
        }.bind(this))
    }

    _respondOnToken(respond) {
        serverCall('/api/gathering-invites/respond-on-token.json', {
            token: this.props.match.params.token,
            respond: respond
        }, function(rsp) {
            this.setState({
                mode: 'TOKEN_RSP'
            })
            if (rsp.status === 'ok') {
                if (respond === 'rejected') {
                    this.setState({
                        respond: 'rejected',
                        msg: rsp.msg
                    })
                } else if (respond === 'accepted') {
                    this.context.router.history.push('/gathering/' + this.state.gatheringInvite.gatheringId)
                }
            } else {
                this.setState({
                    msg: rsp.msg
                })
            }
        }.bind(this))
    }

    _handleContinueToApp() {
        this.context.router.history.push('/app')
    }

    render() {
        if (this.state.mode === 'TOKEN_RSP' && this.state.respond === 'rejected') {
            return (
                <div>
                    <div>{this.state.msg}</div>
                    <a onClick={this._handleContinueToApp}>Continue to the app</a>
                </div>
            )
        }

        if (this.state.mode === 'ASK_FOR_RESPOND' && this.state.gatheringInvite) {
            return (
                <div>
                    <div>
                        <strong>Would you like to join {this.state.gathering.title}?</strong>
                    </div>
                    <div>
                        <a onClick={this._respondOnToken.bind(this, 'accepted')}>Yes</a> <a onClick={this._respondOnToken.bind(this, 'rejected')}>No</a>
                    </div>
                    <div>{this.state.msg}</div>
                </div>
            )
        }

        return (
            <div>{this.state.msg}</div>
        )
    }
}

GatheringInvite.contextTypes = {
  router: PropTypes.object.isRequired
}

GatheringInvite.propTypes = {
    params: PropTypes.object.isRequired,
    areacodes: PropTypes.array.isRequired
}

const mapStoreToProps = function(store) {
    return {
        areacodes: store.areacodesStore
    }
}

export default connect(mapStoreToProps)(GatheringInvite)
