import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { serverCall } from '../../utils'
import { Link } from 'react-router-dom'
import store from '../../store'
import * as types from '../../action-types'

class PasswordRecovery extends Component {
    constructor() {
        super()
        this.state = {
            mode: 'INITIALIZATION',
            msg: 'Processing token in progress...'
        }

        this._handleSetPassword = this._handleSetPassword.bind(this)
    }

    componentWillMount() {
        serverCall('/api/auth/check-recovery-token.json', {
            token: this.props.match.params.token
        }, function(rsp) {
            if (rsp.status === 'ok') {
                this.setState({
                    mode: 'TOKEN_VALID',
                    msg: ''
                })
            } else {
                this.setState({
                    msg: rsp.msg
                })
            }
        }.bind(this))
    }

    _handleSetPassword(e) {
        e.preventDefault
        if (e.keyCode && e.keyCode !== 13) {
            return
        }

        this.setState({
            msg: ''
        })

        serverCall('/api/auth/password-set.json', {
            token: this.props.match.params.token,
            newPassword: this.refs['new-password'].value,
            passwordRepeat: this.refs['password-repeat'].value
        }, function(rsp) {
            if (rsp.status === 'ok') {
                this.setState({
                    mode: 'NEW_PASSWORD_SET',
                    msg: rsp.msg
                })
                
                store.dispatch({
                    type: types.USER_AUTH_STORE,
                    data: {
                        phone: rsp.data.user.phone
                    }
                })

            } else {
                this.setState({
                    msg: rsp.msg
                })
            }
        }.bind(this))
    }

    render() {
        if (this.state.mode === 'TOKEN_VALID') {
            return (
                <div>
                    <div>
                        <label>
                            <span>New Password</span>
                            <input className="form-block" type="password" placeholder="New Password" ref="new-password" onKeyDown={this._handleSetPassword} />
                        </label>
                    </div>
                    <div>
                        <label>
                            <span>Password Repeat</span>
                            <input className="form-block"  placeholder="Password Repeat" type="password" ref="password-repeat" onKeyDown={this._handleSetPassword} />
                        </label>
                    </div>
                    <div>
                        <a className="pretty-btn" onClick={this._handleSetPassword}>Set Password</a>
                    </div>
                    <div>{this.state.msg}</div>
                </div>
            )
        }

        if (this.state.mode === 'NEW_PASSWORD_SET') {
            return (
                <div>
                    <div>{this.state.msg}</div>
                    <div>You can proceed to <Link to="/login">login</Link> now.</div>
                </div>
            )
        }

        return (
            <div>{this.state.msg}</div>
        )
    }
}

PasswordRecovery.propTypes = {
    params: PropTypes.object.isRequired
}

export default PasswordRecovery
