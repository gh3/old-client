import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { serverCall } from '../../utils'
import store from '../../store'
import * as types from '../../action-types'
import { connect } from 'react-redux'
import { findIndex } from 'lodash'

class Trash extends Component {
    constructor() {
        super()

        this.state = {
            removedPhotos: []
        }

        this.xhrs = {}

        this._permanentlyRemovePhoto = this._permanentlyRemovePhoto.bind(this)
        this._renderPhotoName = this._renderPhotoName.bind(this)
        this._renderPhoto = this._renderPhoto.bind(this)
    }

    componentWillMount() {
        serverCall('/api/gathering-photos/find-removed.json', {}, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.TRASH_PHOTOS_RECEIVED,
                    data: rsp.data.gatheringPhotos
                })
            }
        }.bind(this))
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.removedPhotos !== nextProps.removedPhotos) {

            this.setState({
                removedPhotos: nextProps.removedPhotos
            })

            nextProps.removedPhotos.forEach(gatheringPhoto => {
                if (!this.xhrs[gatheringPhoto._id]) {
                    this._requestImageSource(gatheringPhoto)
                }
            })
        }
    }

    _requestImageSource(gatheringPhoto) {
        // https://stackoverflow.com/questions/12710001/how-to-convert-uint8-array-to-base64-encoded-string
        let xhr = new XMLHttpRequest()
        this.xhrs[gatheringPhoto._id] = xhr

        let photoCropURL = process.env.STATIC_BASE_URL + '/photo/' + gatheringPhoto.guid + '_300x300.jpg'

        xhr.open('GET', photoCropURL, true);
        xhr.setRequestHeader('Session', localStorage.getItem('session'));
        xhr.responseType = 'arraybuffer';

        let arrayBuffer, u8, b64encoded, mimetype, gatheringPhotos, gatheringPhotoIndex

        xhr.onload = function () {
            var arrayBuffer = xhr.response
            if (arrayBuffer) {
                u8 = new Uint8Array(arrayBuffer)
                b64encoded = btoa(String.fromCharCode.apply(null, u8))
                mimetype = 'image/png'
                gatheringPhoto.src = 'data:' + mimetype + ';base64,' + b64encoded

                gatheringPhotos = this.state.removedPhotos
                gatheringPhotoIndex = findIndex(gatheringPhotos, { _id: gatheringPhoto._id })
                gatheringPhotos[gatheringPhotoIndex] = gatheringPhoto

                this.setState({
                    removedPhotos: gatheringPhotos
                })
            }
        }.bind(this)

        xhr.send(null);
    }

    _permanentlyRemovePhoto(gatheringPhotoId) {
        serverCall('/api/gathering-photos/remove-one.json', {
            gatheringPhotoId: gatheringPhotoId
        }, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.TRASH_PHOTO_REMOVED,
                    gatheringPhotoId: gatheringPhotoId
                })
            }
        }.bind(this))
    }

    _renderPhotoName(gatheringPhoto) {
        let photoName = gatheringPhoto.customName ? gatheringPhoto.customName : gatheringPhoto.name

        return (
            <div>
                <a onClick={this._permanentlyRemovePhoto.bind(this, gatheringPhoto._id)}>X</a> <strong className="gathering-photo-name">{photoName}</strong>
            </div>
        )
    }

    _renderPhoto(gatheringPhoto) {
        return (
            <div key={gatheringPhoto._id}>
                <img src={gatheringPhoto.src} className="gathering-photo" />
                {this._renderPhotoName(gatheringPhoto)}
            </div>
        )
    }

    render() {
        return (
            <div>
                <span>Trash</span>
                <div>
                    {this.state.removedPhotos.map(this._renderPhoto)}
                </div>
            </div>
        )
    }
}

Trash.propTypes = {
    removedPhotos: PropTypes.array.isRequired
}

const mapStoreToProps = function(store) {
    return {
        removedPhotos: store.trashStore.removedPhotos
    }
}

export default connect(mapStoreToProps)(Trash)
