import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { serverCall } from '../../utils'
import store from '../../store'
import { connect } from 'react-redux'
import * as types from '../../action-types'

class Dashboard extends Component {
    componentDidMount() {
        serverCall('/api/gatherings/find-where-participate.json', {}, function(rsp) {
            if (rsp.status === 'ok') {
                store.dispatch({
                    type: types.GATHERINGS_WHERE_PARTICIPATE,
                    gatherings: rsp.data.gatherings
                })
            }
        }.bind(this))
    }

    _handleCreateGathering() {
        store.dispatch({
            type: types.POPUP_OPEN,
            variant: 'CREATE_GATHERING'
        })
    }

    render() {
        return (
            <div>
                <h1>Welcome to the app!</h1>
                <div>{this.props.gatherings.length ? '' : 'You don\'t have any gatherings yet.'}</div>
                <a className="new-gathering-btn" onClick={this._handleCreateGathering}>Create New Gathering</a>
                <div>{this.props.gatherings.length ? 'Here are your gatherings:' : ''}</div>
                <div>
                    {this.props.gatherings.map(function(gathering) {
                        return (
                            <div key={gathering._id}>
                                <Link to={'/gathering/' + gathering._id} className="gathering-list">{gathering.title}</Link>
                                { gathering.removingDate ? (<strong style={{marginLeft: '10px'}}>removed soon</strong>) : ''}
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

Dashboard.propTypes = {
    gatherings: PropTypes.array.isRequired
}

const mapStoreToProps = function(store) {
    return {
        gatherings: store.gatheringsStore.gatherings
    }
}

export default connect(mapStoreToProps)(Dashboard)
