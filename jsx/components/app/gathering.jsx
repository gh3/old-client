import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { serverCall } from '../../utils'
import store from '../../store'
import * as types from '../../action-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Switch, Route } from 'react-router-dom'
import routes from '../../routes'
import { find } from 'lodash'

class Gathering extends Component {
    constructor() {
        super()
        this.state = {
            mode: 'INITIALIZATION',
            msg: 'Loading gathering...'
        }
    }

    componentWillMount() {
        if (this.props.match.params.gatheringId) {
            serverCall('/api/gatherings/find-one.json', {
                gatheringId: this.props.match.params.gatheringId
            }, function(rsp) {
                if (rsp.status === 'ok') {
                    this.setState({
                        mode: 'GATHERING_OK'
                    })
                    store.dispatch({
                        type: types.GATHERING_DATA_RECEIVED,
                        data: rsp.data.gathering
                    })
                } else {
                    this.setState({
                        mode: 'ERROR',
                        msg: rsp.msg
                    })
                }
            }.bind(this))
        }
    }

    componentWillUnmount() {
        store.dispatch({
            type: types.GATHERING_WILL_UNMOUNT
        })
    }

    renderRoute(route) {
        return (
            <Route key={route.id} path={route.path} component={route.component} exact={!!route.exact} />
        )
    }

    render() {
        if (this.state.mode === 'GATHERING_OK') {
            const gatheringRoutes = find(routes, {id: 'gathering'}).routes
            return (
                <div>
                    <div>Gathering: <strong><Link to={'/gathering/' + this.props.gathering._id}>{this.props.gathering.title}</Link></strong></div>
                    <hr />
                    <Switch>
                        {gatheringRoutes.map(route => this.renderRoute(route))}
                    </Switch>
                </div>
            )
        }

        return (
            <div>{this.state.msg}</div>
        )
    }
}

Gathering.propTypes = {
    gathering: PropTypes.object.isRequired
}

const mapStoreToProps = function(store) {
    return {
        gathering: store.gatheringStore.gathering
    }
}

export default connect(mapStoreToProps)(Gathering)
