import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'
import * as utils from './utils'
import * as types from './action-types'
import { HashRouter, Route } from 'react-router-dom'
import MainLayout from './components/structure/main-layout'

// Before anything starts happening in the app, we first need to check if user
// has session saved in the local storage. If so, dispatch it to user redcurer.
const session = localStorage.getItem('session');
if (session) {
    store.dispatch({
        type: types.USER_AUTHENTICATED,
        session: session
    })
}

// Provider is a top-level component that wrapps our entire application, including
// the Router. We pass it a reference to the store so we can use react-redux's
// connect() method for Component Containers.
render(
    <Provider store={store}>
        <HashRouter>
            <Route path="/" component={MainLayout} />
        </HashRouter>
    </Provider>,
    document.getElementById('root')
)
