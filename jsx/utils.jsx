/*eslint-disable no-var, one-var */
import fetch from 'isomorphic-fetch'
import { polyfill } from 'es6-promise'
polyfill()

export const serverCall = function(url, data, cb) {
    var fetchUrl = process.env.API_BASE_URL + url
    fetch(fetchUrl, {
        method: 'post',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
            'Session': localStorage.getItem('session')
        },
        body: JSON.stringify(data)
    }).then(rsp => {
        return rsp.json()
    }).then(rsp => {
        cb(rsp)
    })
}

export const generateGuid = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8)
        return v.toString(16)
    })
}
