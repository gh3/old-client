// structure
import NotFound                 from './components/structure/not-found'

// tokens
import TokenGatheringInvite     from './components/tokens/gathering-invite'
import TokenPasswordRecovery    from './components/tokens/password-recovery'

// auth
import Login                    from './components/auth/login'
import Register                 from './components/auth/register'
import Password                 from './components/auth/password'
import Logout                   from './components/auth/logout'

// app
import Settings                 from './components/app/settings'
import Gathering                from './components/app/gathering'
import Dashboard                from './components/app/dashboard'
import Trash                    from './components/app/trash'

// gathering
import Photos                   from './components/gathering/photos'
import Options                  from './components/gathering/options'
import Preferences              from './components/gathering/preferences'
import Participants             from './components/gathering/participants'
import Invites                  from './components/gathering/invites'

export default [
    {
        id: 'dashboard',
        path: '/app',
        component: Dashboard,
        type: 'SESSION'
    }, {
        id: 'settings',
        path: '/settings',
        component: Settings,
        type: 'SESSION'
    }, {
        id: 'gathering',
        path: '/gathering/:gatheringId',
        component: Gathering,
        type: 'SESSION',
        routes: [
            {
                id: 'gathering-photos',
                path: '/gathering/:gatheringId',
                component: Photos,
                type: 'SESSION',
                exact: true
            }, {
                id: 'gathering-options',
                path: '/gathering/:gatheringId/options',
                component: Options,
                type: 'SESSION'
            }, {
                id: 'gathering-preferences',
                path: '/gathering/:gatheringId/preferences',
                component: Preferences,
                type: 'SESSION'
            }, {
                id: 'gathering-participants',
                path: '/gathering/:gatheringId/participants',
                component: Participants,
                type: 'SESSION'
            }, {
                id: 'gathering-invites',
                path: '/gathering/:gatheringId/invites',
                component: Invites,
                type: 'SESSION'
            }
        ]
    }, {
        id: 'trash',
        path: '/trash',
        component: Trash,
        type: 'SESSION'
    }, {
        id: 'token-gathering-invite',
        path: '/gi/:token',
        component: TokenGatheringInvite,
        type: 'TOKEN'
    }, {
        id: 'token-password-recovery',
        path: '/pr/:token',
        component: TokenPasswordRecovery,
        type: 'TOKEN'
    }, {
        id: 'login',
        path: '/login',
        component: Login,
        type: 'REGULAR'
    }, {
        id: 'register',
        path: '/register',
        component: Register,
        type: 'REGULAR'
    }, {
        id: 'password',
        path: '/password',
        component: Password,
        type: 'REGULAR'
    }, {
        id: 'logout',
        path: '/logout',
        component: Logout,
        type: 'REGULAR'
    }, {
        id: 'not-found',
        path: '*',
        component: NotFound,
        type: 'REGULAR'
    }
]