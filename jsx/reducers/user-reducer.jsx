import * as types from '../action-types'
import * as utils from '../utils'

const initialUserStore = {
    auth: {},
    session: '',
    whoami: {}
}

const userReducer = function(store = initialUserStore, action) {
    switch(action.type) {
        case types.STORE_RESET: {
            localStorage.removeItem('session');
            return initialUserStore
        }

        case types.USER_AUTHENTICATED: {
            return { ...store, session: action.session }
        }

        case types.USER_AUTH_STORE: {
            return { ...store, auth: action.data }
        }

        case types.USER_SESSION_SET: {
            localStorage.setItem('session', action.session);
            return { ...store, auth: initialUserStore.auth, session: action.session }
        }

        case types.USER_SESSION_UNSET: {
            localStorage.removeItem('session');
            return { ...store, session: initialUserStore.session, whoami: initialUserStore.whoami }
        }

        case types.USER_WHOAMI_RECEIVED: {
            return { ...store, whoami: action.whoami }
        }

        case types.USER_NAME_UPDATED: {
            const whoami = store.whoami
            whoami.name = action.name
            return { ...store, whoami: whoami }
        }
    }
    return store
}

export default userReducer
