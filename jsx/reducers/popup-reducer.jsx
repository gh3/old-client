import * as types from '../action-types'

const initialPopupStore = {
    visible: false,
    variant: '',
    data: {},
    buttons: []
}

const popupReducer = function(store = initialPopupStore, action) {
    switch(action.type) {
        case types.STORE_RESET: {
            return initialPopupStore
        }

        case types.POPUP_OPEN: {
            return { ...store,
                visible: true,
                variant: action.variant,
                data: action.data,
                buttons: action.buttons
            }
        }

        case types.POPUP_CLOSE: {
            return initialPopupStore
        }

        case types.POPUP_BUTTONS_SET: {
            return { ...store, buttons: action.buttons }
        }

        case types.POPUP_DATA_SET: {
            return { ...store, data: action.data }
        }
    }
    return store
}

export default popupReducer
