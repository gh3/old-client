import { combineReducers } from 'redux'

// import reducers
import userReducer from './user-reducer'
import gatheringReducer from './gathering-reducer'
import gatheringsReducer from './gatherings-reducer'
import popupReducer from './popup-reducer'
import areacodesReducer from './areacodes-reducer'
import trashReducer from './trash-reducer'

// combine reducers
const reducers = combineReducers({
    userStore: userReducer,
    gatheringStore: gatheringReducer,
    gatheringsStore: gatheringsReducer,
    popupStore: popupReducer,
    areacodesStore: areacodesReducer,
    trashStore: trashReducer
})

export default reducers
