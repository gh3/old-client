import * as types from '../action-types'

const initialGatheringStore = {
    gathering: {},
    gatheringPhotos: [],
    gatheringParticipants: [],
    gatheringInvites: []
}

const gatheringReducer = function(store = initialGatheringStore, action) {
    switch(action.type) {
        case types.STORE_RESET: {
            return initialGatheringStore
        }

        case types.GATHERING_DATA_RECEIVED: {
            return { ...store, gathering: action.data }
        }

        case types.GATHERING_PHOTOS_RECEIVED: {
            return { ...store, gatheringPhotos: action.data }
        }

        case types.GATHERING_PHOTO_INSERTED: {
            const gatheringPhotos = [].concat(store.gatheringPhotos)
            gatheringPhotos.unshift(action.gatheringPhoto)
            return { ...store, gatheringPhotos: gatheringPhotos }
        }

        case types.GATHERING_PHOTO_NAME_UPDATED: {
            const gatheringPhotos = [].concat(store.gatheringPhotos)
            const i = gatheringPhotos.findIndex(elm => elm._id === action.data.gatheringPhotoId)
            gatheringPhotos[i].customName = action.data.customName
            return { ...store, gatheringPhotos: gatheringPhotos }
        }

        case types.GATHERING_PHOTO_REMOVED: {
            const gatheringPhotos = store.gatheringPhotos.filter(function(elm) {
                return elm._id !== action.gatheringPhotoId
            })
            return { ...store, gatheringPhotos: gatheringPhotos }
        }

        case types.GATHERING_PARTICIPANTS_RECEIVED: {
            return { ...store, gatheringParticipants: action.data }
        }

        case types.GATHERING_PARTICIPANT_REMOVED: {
            const gatheringParticipants = store.gatheringParticipants.filter(function(elm) {
                return elm._id !== action.userId
            })
            return { ...store, gatheringParticipants: gatheringParticipants }
        }

        case types.GATHERING_INVITES_RECEIVED: {
            return { ...store, gatheringInvites: action.data }
        }

        case types.GATHERING_INVITE_REMOVED: {
            const gatheringInvites = store.gatheringInvites.filter(function(elm) {
                return elm._id !== action.gatheringInviteId
            })
            return { ...store, gatheringInvites: gatheringInvites }
        }

        case types.GATHERING_INVITE_INSERTED: {
            const gatheringInvites = [].concat(store.gatheringInvites)
            gatheringInvites.push(action.gatheringInvite)
            return { ...store, gatheringInvites: gatheringInvites }
        }

        case types.GATHERING_WILL_UNMOUNT: {
            return initialGatheringStore
        }

        case types.GATHERING_REMOVING_SET: {
            const gathering = store.gathering
            gathering.removingDate = action.removingDate
            return { ...store, gathering: gathering }
        }

        case types.GATHERING_REMOVING_UNSET: {
            const gathering = store.gathering
            delete gathering.removingDate
            return { ...store, gathering: gathering }
        }
    }
    return store
}

export default gatheringReducer
