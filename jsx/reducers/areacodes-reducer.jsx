import * as types from '../action-types'

const initialAreacodesStore = []

const areacodesReducer = function(store = initialAreacodesStore, action) {
    switch(action.type) {
        case types.STORE_RESET: {
            return initialAreacodesStore
        }

        case types.AREACODES_DATA_RECEIVED: {
            return [ ...store, ...action.areacodes ]
        }
    }
    return store
}

export default areacodesReducer
