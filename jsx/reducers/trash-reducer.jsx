import * as types from '../action-types'

const initialTrashStore = {
    removedPhotos: []
}

const trashReducer = function(store = initialTrashStore, action) {
    switch(action.type) {
        case types.STORE_RESET: {
            return initialTrashStore
        }

        case types.TRASH_PHOTOS_RECEIVED: {
            return { ...store, removedPhotos: action.data }
        }

        case types.TRASH_PHOTO_REMOVED: {
            const removedPhotos = store.removedPhotos.filter(function(elm) {
                return elm._id !== action.gatheringPhotoId
            })
            return { ...store, removedPhotos: removedPhotos }
        }
    }
    return store
}

export default trashReducer
