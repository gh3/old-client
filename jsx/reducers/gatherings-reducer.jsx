import * as types from '../action-types'

const initialGatheringsStore = {
    gatherings: []
}

const gatheringsReducer = function(store = initialGatheringsStore, action) {
    switch(action.type) {
        case types.STORE_RESET: {
            return initialGatheringsStore
        }

        case types.GATHERINGS_WHERE_PARTICIPATE: {
            return { ...store, gatherings: action.gatherings }
        }

        case types.GATHERINGS_ONE_REMOVED: {
            const gatherings = store.gatherings.filter(function(elm) {
                return elm._id !== action.gatheringId
            })
            return { ...store, gatherings: gatherings }
        }

        case types.GATHERING_REMOVING_SET: {
            const gatherings = store.gatherings.map(function(elm) {
                if (elm._id === action.gatheringId) {
                    elm.removingDate = action.removingDate
                }
                return elm
            })
            return { ...store, gatherings: gatherings }
        }

        case types.GATHERING_REMOVING_UNSET: {
            const gatherings = store.gatherings.map(function(elm) {
                if (elm._id === action.gatheringId) {
                    delete elm.removingDate
                }
                return elm
            })
            return { ...store, gatherings: gatherings }
        }
    }
    return store
}

export default gatheringsReducer
