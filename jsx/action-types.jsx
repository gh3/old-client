// general
export const STORE_RESET                     = 'STORE_RESET'

// user
export const USER_AUTHENTICATED              = 'USER_AUTHENTICATED'
export const USER_AUTH_STORE                 = 'USER_AUTH_STORE'
export const USER_SESSION_SET                = 'USER_SESSION_SET'
export const USER_SESSION_UNSET              = 'USER_SESSION_UNSET'
export const USER_WHOAMI_RECEIVED            = 'USER_WHOAMI_RECEIVED'
export const USER_NAME_UPDATED               = 'USER_NAME_UPDATED'

// popup
export const POPUP_OPEN                      = 'POPUP_OPEN'
export const POPUP_CLOSE                     = 'POPUP_CLOSE'
export const POPUP_BUTTONS_SET               = 'POPUP_BUTTONS_SET'
export const POPUP_DATA_SET                  = 'POPUP_DATA_SET'

// gatherings
export const GATHERINGS_WHERE_PARTICIPATE    = 'GATHERINGS_WHERE_PARTICIPATE'
export const GATHERINGS_ONE_REMOVED          = 'GATHERINGS_ONE_REMOVED'

// gathering
export const GATHERING_DATA_RECEIVED         = 'GATHERING_DATA_RECEIVED'
export const GATHERING_PHOTOS_RECEIVED       = 'GATHERING_PHOTOS_RECEIVED'
export const GATHERING_PHOTO_INSERTED        = 'GATHERING_PHOTO_INSERTED'
export const GATHERING_PHOTO_NAME_UPDATED    = 'GATHERING_PHOTO_NAME_UPDATED'
export const GATHERING_PHOTO_REMOVED         = 'GATHERING_PHOTO_REMOVED'
export const GATHERING_PARTICIPANTS_RECEIVED = 'GATHERING_PARTICIPANTS_RECEIVED'
export const GATHERING_PARTICIPANT_REMOVED   = 'GATHERING_PARTICIPANT_REMOVED'
export const GATHERING_INVITES_RECEIVED      = 'GATHERING_INVITES_RECEIVED'
export const GATHERING_INVITE_REMOVED        = 'GATHERING_INVITE_REMOVED'
export const GATHERING_INVITE_INSERTED       = 'GATHERING_INVITE_INSERTED'
export const GATHERING_WILL_UNMOUNT          = 'GATHERING_WILL_UNMOUNT'
export const GATHERING_REMOVING_SET          = 'GATHERING_REMOVING_SET'
export const GATHERING_REMOVING_UNSET        = 'GATHERING_REMOVING_UNSET'

// areacodes
export const AREACODES_DATA_RECEIVED         = 'AREACODES_DATA_RECEIVED'

// trash
export const TRASH_PHOTOS_RECEIVED           = 'TRASH_PHOTOS_RECEIVED'
export const TRASH_PHOTO_REMOVED             = 'TRASH_PHOTO_REMOVED'
