var webpack = require('webpack');

var baseConfig = {
    entry: "./jsx/app.jsx",
    module: {
        loaders: [{
            loader: 'babel-loader',
            exclude: /node_modules/,
            query: {
                presets: ['react', 'es2015', 'stage-2']
            }
        }]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    }
}

var clientConfig = Object.assign({}, baseConfig, {
    output: {
        filename: "./public/js/js-build.js"
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                API_BASE_URL: '""',
                STATIC_BASE_URL: '""'
            }
        })
    ]
})

var phonegapConfig = Object.assign({}, baseConfig, {
    output: {
        filename: "../phonegap/gathering/www/js/js-build.js"
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                API_BASE_URL: '"http://gh3.co"',
                STATIC_BASE_URL: '"http://gh3.co"'
            }
        })
    ]
})

module.exports = [clientConfig, phonegapConfig]
