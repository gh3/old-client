This project is obsolete and not maintained anymore!

# PREPERATION #

First you need to set up your local machine.

1.
Install all necessary node modules which are listed under "dependencies" in package.json.
You can do it with this command:
```
npm install
```

Running this command as **root/administrator/sudo** will be probably requested.

2.
Check if any packages are outdated on your machine:
```
npm outdated
```

In case there is outdated package which has "Current" version different than "Wanted", you need to updated it with command:
```
npm update
```

If nothing was returned, you can go ahead.

3.
Your client is ready for building!




# BUILDING JAVASCRIPT #

There is only one commnad which you need to run to start working.

**Tip:** *Commad below starts webpack as you can also see it in package.json file under "scripts".*

1.
Process which watch for all your changes and output **public/js/js-build.js** file can be started as:
```
npm run build
```
If everything looks okay, you can start with development. Don't forget that you need to start your server as well on the backend.


2.
We are using minimazed JS build on production. To build the minimazed JS run:
```
npm run build-min
```
Minimized version of build is placed on the same place as regular one, here: **public/js/js-build.js**.




# WEBPACK #

More about the webpack configuration you will find **webpack.config.js** file. Short overview goes like this. Webpack takes root component which is defined under "entry" and runs it through the babel transformations defined under "module.loaders.query.presets" and splits out the bundle defined under "output.filename". For minimizing and compressing we are using UglifyJsPlugin, but we need to use NODE_ENV as "production" otherwise we get errors in browser console.